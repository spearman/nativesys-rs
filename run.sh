#!/usr/bin/env bash

set -x

cargo run --example example --features="math-utils rs-utils"

exit 0
