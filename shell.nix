with import <nixpkgs> {};
mkShell {
  buildInputs = [
    gdb # required for rust-gdb
    llvm
    rustup
    rust-analyzer
  ];
  # required for opengl
  LD_LIBRARY_PATH = lib.makeLibraryPath [
    libglvnd
    xorg.libX11
    xorg.libXcursor
    xorg.libXi
    xorg.libXrandr
    libxkbcommon
  ]
  # needed for libfmod.so to dynamically link libstdc++.so.6, libfmod.so.10, and
  # libalsa.so
    + "${stdenv.cc.cc.lib}/lib:${alsaLib}/lib"
    + ":workspace/fmod-rs/fmod-sys/lib/x86_64";
  # needed for fmod-sys crate so bindgen can find libclang.so
  LIBCLANG_PATH="${llvmPackages.libclang.lib}/lib";
}
