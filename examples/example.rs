use nsys;

fn main() {
  println!("nativesys example main...");
  let p = nsys::math::Point2::new (1, 0);
  nsys::show!(p);
  println!("...nativesys example main");
}
