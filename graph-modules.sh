#!/bin/sh

set -x

cargo modules generate graph --package nativesys > modules.dot && \
  dot -Tpng modules.dot > modules.png && feh modules.png

exit
