//! # <IMG STYLE="vertical-align: middle" SRC="https://gitlab.com/spearman/nativesys-rs/-/raw/master/doc/nativesys.png">
//!
//! *Native Systems Rust namespace*

#[cfg(feature = "rs-utils")]
pub use rs_utils::*;
#[cfg(feature = "color-utils")]
pub use color_utils as color;
#[cfg(feature = "curses-utils")]
pub use curses_utils as curses;
#[cfg(feature = "fmod-utils")]
pub use fmod_utils as fmod;
#[cfg(feature = "gl-utils")]
pub use gl_utils as gl;
#[cfg(feature = "math-utils")]
pub use math_utils as math;
#[cfg(feature = "math-utils")]
pub use math_utils::geometry as geometry;
#[cfg(feature = "signal-utils")]
pub use signal_utils as signal;
