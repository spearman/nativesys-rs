#!/bin/sh

set -x

cargo doc --no-deps --workspace --all-features --open

exit 0
